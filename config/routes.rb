Rails.application.routes.draw do
  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :home
  
  resources :home do 

  	member do
  	  post 'new_patient' 
  	end
  	member do 
  	  post 'new_doctor'
  	end
  	member do 
  	  post 'new_appointment'
  	end
  	member do 
  	  get 'appointment_index'
  	end
  	member do 
  	  post 'sign_up'
  	end
  	member do 
  	  post 'sign_in'
  	end
  	member do 
  	  delete 'logout'
  	end
  end
end
