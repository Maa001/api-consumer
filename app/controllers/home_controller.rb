require 'rubygems'
require 'httparty'

class HomeController < ApplicationController

  include HTTParty

  def index
  	# binding.pry
  	# headers = { 'Content-Type' => 'application/json' }
  	# query = {
			#  "user": {
			#             "username": "Mah123e2sh",
			#             "email": "mahesh3242@gmail.com",
			#             "password": "123456"
			            
			#           }
			# }
  	# Consumer.data
  	# binding.pry
  	user = HTTParty.post("https://tstun-api.herokuapp.com/users",headers:{"Content-Type"=>"application/jsonZ"},
  	      query:{"user"=>{"username"=>"Maa0225503hj411","email"=>"msd1334442237834@gmail.com","password"=>"123456"}}.to_h)
  	if user["token"].present?
  	  $token = user["token"]
      @appointments = HTTParty.get("https://tstun-api.herokuapp.com/appointments",
  	  	    headers:{"token"=>"#{$token}","Authorization"=>"Token #{$token}"})
      # binding.pry
  	else

  	end
  end

  def new_patient
  	 create_patient =  HTTParty.post("https://tstun-api.herokuapp.com/patients",
  	    	headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"},
  	    	query:{"patient"=>{"name"=>"Damoo","phone_number"=>"48556666666"}})
  end

  def new_doctor
  	create_doctor = HTTParty.post("https://tstun-api.herokuapp.com/patients",
  	    	headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"},
  	    	query:{"patient"=>{"name"=>"Damoo","phone_number"=>"48556666666"}})
  end

  def new_appointment
  	patient_list = HTTParty.get("https://tstun-api.herokuapp.com/patients",
  	    	headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"})
  	doctor_list = HTTParty.get("https://tstun-api.herokuapp.com/doctors",
  	  	    headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"})

  	create_appointment = HTTParty.post("https://tstun-api.herokuapp.com/appointments",
  	    	headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"},
  	    	query:{	"appointments": {"disease": "Skin"},"patient_id": "1", "doctor_id": "1"})
  end

  def appointment_index
  	appointment_list = HTTParty.get("https://tstun-api.herokuapp.com/appointments",
  	  	    headers:{"token"=>"gN6hmxQScxCDjeacU2s5bFJZ","Authorization"=>"Token gN6hmxQScxCDjeacU2s5bFJZ"})
  	  
  end

  def sign_up
  	user = HTTParty.post("https://tstun-api.herokuapp.com/users",headers:{"Content-Type"=>"application/jsonZ"},
  	      query:{"user"=>{"username"=>"Maa0225503411","email"=>"msd13344422334@gmail.com","password"=>"123456"}}.to_h)
  	if user["token"].present?
  	  $token = user["token"]
      @appointments = HTTParty.get("https://tstun-api.herokuapp.com/appointments",
  	  	    headers:{"token"=>"#{$token}","Authorization"=>"Token #{$token}"})
  	else
  	 
  	end
  end

  def sign_in
  	query = {"user"=>{"email"=>"msd13344422334@gmail.com","password"=>"123456"}}.to_h
  	user = HTTParty.post("https://tstun-api.herokuapp.com/users",headers:{"Content-Type"=>"application/jsonZ"},
  	      query: query)
  	if user["token"].present?
  	  $token = user["token"]
      @appointment_list = HTTParty.get("https://tstun-api.herokuapp.com/appointments",
  	  	    headers:{"token"=>"#{$token}","Authorization"=>"Token #{$token}"})
  	else
  	 
  	end
  end

  def logout
  end


end
