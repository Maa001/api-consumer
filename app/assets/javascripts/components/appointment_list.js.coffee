@Appointments = React.createClass
  getInitialState: ->
    appointments: @props.data
  getDefaultProps: ->
    appointments: []
  addLift: (lift) ->
    appointments = @state.appointments.slice()
    appointments.push appointment
    @setState appointments: appointments
  render: ->
    React.DOM.div
      className: 'appointments'
      React.DOM.h1
        className: 'title'
         'Appointments'
      React.createElement AppointmentForm, handleNewAppointment: @addAppointment
      React.DOM.table
        className: 'table table-bordered'
        React.DOM.thead null
            React.DOM.th null, 'UserName'
            React.DOM.th null, 'Email'
            React.DOM.th null, 'Password'
          React.DOM.tbody null,
            for appointment in @state.appointments
              React.createElement Appointment, key: appointment.id, appointment.disease