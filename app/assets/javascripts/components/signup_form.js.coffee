@SignUpForm = React.createClass
  getInitialState: ->
    username: ''
    email: ''
    password: ''
  handleValueChange: (e)
    valueName = e.target.name
    @setState "#{ valueName }": e.target.value
  valid: ->
    @state.username && @state.email && @sate.password
  handleSubmit: (e) ->
    e.preventDefault()
    $.post '', {user: @state}, (data) => 
      @props.handleNewLift data
      @setState  @getInitialState(), 'JSON'
  render: ->
    React.DOM.form
      className: 'form-inline'
      onSubmit: @handleSubmit
      React.DOM.div
        className: 'form-group'
        React.DOM.input
          type: 'string'
          className: 'form-control'
          placeholder: 'Enter User name'
          name: 'username'
          value: @state.username
          onChange: @handleValueChange
        React.DOM.input
          type: 'string'
          className: 'form-control'
          placeholder: 'Enter Email'
          name: 'email'
          value: @state.email
          onChange: @handleValueChange
        React.DOM.input
          type: 'string'
          className: 'form-control'
          placeholder: 'Enter password'
          name: 'password'
          value: @state.password
          onChange: @handleValueChange
        React.DOM.button
          type: 'submit'
          className: 'btn btn-primary'
          disabled: !@valid()
          'Sign Up'
